import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import Alert from "ui/build/components/Alert/Alert";
import EditUserProfile from './EditUserProfile/EditUserProfile';
import { getUserList, updateUserDataByID } from './EditUserProfile/EditUserProfile.service';
import PopOver from './PopOver';

import browserBunyan from "rr-js/lib/browserBunyan.js";
import { v4 as uuid } from "uuid";
import { SERVICE_NAME } from "../../../utils/StaticData";

const UserProfile = () => {
    const logger = browserBunyan({ name: SERVICE_NAME });
    const history = useHistory();
    const [userData, setUserData] = useState();
    const [selectedAvatar, setSelectedAvatar] = useState();
    const [showProfileDialog, setShowProfileDialog] = useState(false);
    const [showAlert, setShowAlert] = useState(false);
    const [disableBtn, setDisableBtn] = useState(true);
    const [toast, setToast] = useState({
        alertType: "",
        message: "",
    });
    const [profileDialogOptions, setProfileDialogOptions] = useState({
        heading: "Create new mail",
        submitText: "Create",
        cancelText: "Cancel",
        contentText: "",
        onSubmit: "",
        onCancel: "",
        size: "md"
    });

    const userInfoFun = () => {
        getUserList({
            userId: JSON.parse(localStorage.getItem('loginResponse')).responseData
                .userId,
        })
            .then(res => {
                const rrid = uuid();
                logger.info(rrid, SERVICE_NAME, "Get user profile API success", {
                    userId: JSON.parse(localStorage.getItem('loginResponse')).responseData.userId
                });
                res.data.data[0].userPicURL ? setSelectedAvatar(res.data.data[0].userPicURL) : setSelectedAvatar("")
                setUserData(res.data.data[0]);
            })
            .catch(err => {
                const rrid = uuid();
                logger.error(rrid, SERVICE_NAME, "Get user profile API error", {
                    userId: JSON.parse(localStorage.getItem('loginResponse')).responseData.userId,
                    error: err
                });
                if (err.response) {
                    const statusCode = err.response.status;
                    if (statusCode === 401) {
                        history.push('/')
                        localStorage.clear()
                        window.location.reload();
                    }
                }
            });
    }

    useEffect(() => {
        userInfoFun();
    }, [])

    const openEditProfile = () => {
        setProfileDialogOptions({
            ...profileDialogOptions,
            heading: "Edit Your Profile",
            submitText: "Save Changes",
            cancelText: "Cancel",
            onCancel: closeDialog,
            onSubmit: handleSubmit
        });
        setShowProfileDialog(true);
    }

    const handleSubmit = (data) => {
        updateUserDataByID(data)
            .then(resp => {
                if (resp.data.statusCode === 200) {
                    const rrid = uuid();
                    logger.info(rrid, SERVICE_NAME, "Update user profile API success", {
                        userId: JSON.parse(localStorage.getItem('loginResponse')).responseData.userId
                    });
                    setShowProfileDialog(false);
                    setShowAlert(true)
                    setToast({
                        alertType: 'success',
                        message: resp.data.message
                    });
                    userInfoFun();
                }
            })
            .catch(err => {
                const rrid = uuid();
                logger.error(rrid, SERVICE_NAME, "Update user profile API error", {
                    userId: JSON.parse(localStorage.getItem('loginResponse')).responseData.userId,
                    error: err
                });
                setShowAlert(true);
                setToast({
                    alertType: 'danger',
                    message: err.response.data.message
                });
                const statusCode = err.response.status;
                if (statusCode === 401) {
                    localStorage.clear()
                    history.push('/')
                    window.location.reload();
                };
            })
    }

    const handleProfilePic = (profileName) => {
        setSelectedAvatar(profileName)
        setDisableBtn(false)
    }

    const handleSaveProfilePic = (profileName) => {
        setUserData({
            ...userData,
            userPicURL: profileName
        })
    }

    const handleChange = (e, setFieldValue) => {
        setFieldValue(e.target.name, e.target.value);
        setUserData({
            ...userData,
            [e.target.name]: e.target.value
        })
        setDisableBtn(false);
    }

    const closeDialog = (data) => {
        setShowProfileDialog(false);
        setSelectedAvatar(userData.userPicURL)
        setDisableBtn(false);
    }


    return (
        <React.Fragment>
            {showAlert ? (
                <Alert
                    alertType={toast.alertType}
                    show={showAlert}
                    dismiss={true}
                    onClose={() => {
                        setShowAlert(!showAlert)
                    }
                    }
                >
                    {toast.message}
                </Alert>
            ) : null}

            {userData &&
                <PopOver userData={userData} openEditProfile={openEditProfile} />
            }
            {showProfileDialog && (
                <EditUserProfile
                    onCancel={closeDialog}
                    open={showProfileDialog}
                    options={profileDialogOptions}
                    userData={userData}
                    selectedAvatar={selectedAvatar}
                    handleChangeFun={handleChange}
                    handleProfilePic={handleProfilePic}
                    btnDisable={disableBtn}
                    handleSaveProfilePic={handleSaveProfilePic}
                />
            )}

        </React.Fragment>
    )
}

export default UserProfile

