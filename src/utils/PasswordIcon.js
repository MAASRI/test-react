import React from "react";
import { ImEye, ImEyeBlocked } from "react-icons/im";
import PropTypes from "prop-types";

export default function PasswordIcon({ textConfig, onClick, size }) {
  return textConfig.type === "password" ? (
    <ImEye onClick={() => onClick(textConfig)} size={size} />
  ) : (
      <ImEyeBlocked onClick={() => onClick(textConfig)} size={size} />
    );
}

PasswordIcon.propTypes = {
  textConfig: PropTypes.object,
  onClick: PropTypes.func,
  size: PropTypes.string,
};

PasswordIcon.defaultProps = {
  textConfig: null,
  onClick: null,
  size: null,
};
