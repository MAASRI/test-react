import React, { useState, useEffect } from 'react';
import '../AppHeader.scss';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Para from 'ui/build/components/Para/Para';
import RenderIcon from './Icons/RenderIcon'
import _ from "lodash";
import GroupedNav from './GroupedNav';
import '../AppHeader.scss'

const RheaSideNav = () => {

  const [groupedPermissions, setGroupedPermissions] = useState();
  const [loaded, setLoaded] = useState(false);
  const [isExpanded, setIsExpdanded] = useState(false);
  const [isExpandedByNav, setIsExpdandedByNav] = useState(false);
  const [lockNav, setLockNav] = useState(false);
  const [active, setActive] = useState('');
  const toggleButton = () => {
    setIsExpdanded(!isExpanded);
  };
  const loginReponse = JSON.parse(localStorage.getItem('loginResponse'));
  const loginPermission = loginReponse.responseData.data.roleData.permissions;

  useEffect(() => {
    // Group the elements of Array based on `groupName` property
    const tempPermissions = _.chain(loginPermission)
      .groupBy("groupName")
      .map((groupedPermissions, key) => ({ group: key, name: groupedPermissions }))
      .value();
    setGroupedPermissions(tempPermissions);
  }, []);

  useEffect(() => {
    setLoaded(true);
  }, [groupedPermissions]);

  useEffect(() => {
    setLockNav(isExpanded)
  }, [isExpanded])
  const hoverExpand = () => {
    !lockNav && setIsExpdandedByNav(true)
  }

  const hoverCollapse = () => {
    !lockNav && setIsExpdandedByNav(false)
  }

  const setActiveListItem = (e, item) => {
    setActive(item);
    e.preventDefault();
  };
  return (
    loaded && (
      <nav id={`${isExpanded || isExpandedByNav ? 'openNav' : 'closeNav'}`} className={`${isExpanded || isExpandedByNav ? 'nav' : 'nav-closed'}`}>
        <svg
          onClick={toggleButton}
          className="nav-expand"
          viewBox="0 0 20 20"
        >
          <path d="M11.611,10.049l-4.76-4.873c-0.303-0.31-0.297-0.804,0.012-1.105c0.309-0.304,0.803-0.293,1.105,0.012l5.306,5.433c0.304,0.31,0.296,0.805-0.012,1.105L7.83,15.928c-0.152,0.148-0.35,0.223-0.547,0.223c-0.203,0-0.406-0.08-0.559-0.236c-0.303-0.309-0.295-0.803,0.012-1.104L11.611,10.049z"></path>
        </svg>
        <aside className="" style={{ height: '60px', placeItems: 'center' }}>
          {localStorage.getItem('logoUrl') ?
            <img
              className="brand-logo"
              src={localStorage.getItem('logoUrl')}
              alt="logo"
            /> :
            <img
              className="brand-logo"
              src="https://azure-production.s3.amazonaws.com/DF_Logo.png"
              alt="logo"
            />
          }

          <div className="brand-name">
            <Para style={{ color: 'white' }}>{localStorage.getItem('clientName') ? localStorage.getItem('clientName') : "Datafoundry Pvt Ltd"}</Para>
          </div>
        </aside>
        <div
          className="side-menu"
          style={{ overflowY: 'auto', height: '95vh' }}
        >
          <hr className="separator" />
          <div style={{ padding: '5px 0px' }}></div>
          <div onMouseEnter={hoverExpand} onMouseLeave={hoverCollapse}>
            <ul className="nav-list">
              {groupedPermissions.map((item, index) => {
                return item.group &&
                  item.group.trim() != "" &&
                  item.group != "undefined" ? (
                  <GroupedNav key={index} active={active} RenderIcon={RenderIcon} setActiveListItem={setActiveListItem} item={item} />
                ) : (
                  <div key={index}>
                    {item.name.map((innerItem, index) => (
                      <li
                        key={index}
                        className={`nav-list-item ${active === innerItem.displayName
                          ? 'nav-list-item-active'
                          : ''
                          }`}
                      >
                        <div onClick={e => setActiveListItem(e, innerItem.displayName)}>
                          <Link key={index} to={innerItem.redirectUrl}>
                            <RenderIcon icon={innerItem.icon} />
                            <p>{innerItem.displayName}</p>
                          </Link>
                        </div>
                      </li>
                    ))}
                  </div>
                );
              })}
            </ul>
          </div>
        </div>
      </nav>
    )
  )
}

export default RheaSideNav