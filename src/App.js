import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AppHeader from './components/app-header/AppHeader';
import Login from './components/login/Login';
const App = () => {

  return (
    <BrowserRouter>
      <React.Fragment>
        <Switch>
          {
            JSON.parse(localStorage.getItem('loginResponse')) ?
              <AppHeader /> : <Login />
          }
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  )
};

export default App;
