import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MicroFrontend from '../micro-frontend/MicroFrontend';
import './MainContainer.scss'

const MainContainer = () => {
  const loginReponse = JSON.parse(localStorage.getItem('loginResponse'));
  const loginPermission =
    loginReponse.responseData.data.roleData.permissions;
    
  return (
    <div className="content-wrapper">
      <section className="content-header">
        <Switch>
        {loginPermission.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.redirectUrl}
                component={() => (
                  <MicroFrontend
                    host={route.host}
                    name={route.microServiceName}
                    moduleId={route.moduleId}
                  />
                )}
              />
            );
          })}
        </Switch>
      </section>
    </div>
  );
};

export default MainContainer;