import React, { useState, useCallback } from 'react'

const useFetch = url => {
  const [error, setError] = useState(null);
  const [isPending, setIsPending] = useState(false);
  const [data, setData] = useState(null);
  const executeFetch = useCallback(
    async ({ api, options, onSuccess, onError }) => {
      setIsPending(true);
      setError(null);
      return await fetch(`${url}/${api}`, options)
        .then(response => response.json())
        .then(response => {
          setData(response);
          onSuccess(response);
        })
        .catch(err => {
          setError(err.message);
          onError(err);
        })
        .finally(() => setIsPending(false));
    },
    [url, setIsPending, setError]
  );
  return { executeFetch, error, isPending, data };
};

export default useFetch