import React from 'react';
import './AppHeader.scss';
import { Navbar, Nav } from 'react-bootstrap';
import MainContainer from '../main-container/MainContainer';
import RheaSideNav from './RheaSideNav/RheaSideNav';
import UserProfile from './UserProfile/UserProfile';

const AppHeader = props => {

  return (
    <React.Fragment>
      <div className="global-container">
        <RheaSideNav></RheaSideNav>
        <div className="nav-header-main-content">
          <div className="navbar-header">
            <Navbar>
              <Nav></Nav>
              <UserProfile></UserProfile>
              {/* <Form inline>
                <div className="form-group has-search">
                  <span className="form-control-feedback">
                    <BsSearch className="form-control-feedback" />
                  </span>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Diya is here to help. Ask her anything."
                  />
                </div>
              </Form> */}
            </Navbar>
          </div>
          <div className="main-container">
            <MainContainer />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default AppHeader;
