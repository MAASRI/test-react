import React, { useEffect, useState } from 'react'
import ErrorUI from '../error-page/ErrorUI';

import browserBunyan from "rr-js/lib/browserBunyan.js";
import { v4 as uuid } from "uuid";
import { SERVICE_NAME } from "../../utils/StaticData";

const MicroFrontend = (props) => {
  const logger = browserBunyan({ name: SERVICE_NAME });
  const [errorInfo, seterrorInfo] = useState({
    hasError: false,
    errorMessage: '',
    errorStack: ''
  })

  const renderMicroFrontend = () => {
    const { name, window, history, host } = props;
    try {
      window[`render${name}`](`${name}-container`, history);
    }
    catch (error) {
      seterrorInfo({
        ...errorInfo,
        hasError: true,
        errorMessage: `Please verify host - ${host} and microservice name - ${name}`,
        errorStack: error.stack
      })
    }
  };

  useEffect(() => {
    const { name, host, moduleId, document } = props;
    localStorage.setItem('moduleId', moduleId)
    const scriptId = `micro-frontend-script-${name}`;
    if (document.getElementById(scriptId)) {
      renderMicroFrontend();
      return;
    }
    fetch(`${host}/asset-manifest.json`)
      .then(res => res.json())
      .then(manifest => {
        const rrid = uuid();
        logger.info(rrid, SERVICE_NAME, "Asset manifest API success", { name, host });
        const script = document.createElement('script');
        script.id = scriptId;
        script.crossOrigin = '';
        script.src = `${host}${manifest['main.js']}`;
        script.onload = renderMicroFrontend;
        document.head.appendChild(script);
      }).catch((error) => {
        const rrid = uuid();
        logger.error(rrid, SERVICE_NAME, "Asset manifest API error", { name, host, error });
        seterrorInfo({
          ...errorInfo,
          hasError: true,
          errorMessage: `Unable to send the request, verify the host name - ${host}`,
          errorStack: error.stack
        })
      })
  }, [])

  return (
    errorInfo.hasError === true ? <ErrorUI errorMessage={errorInfo.errorMessage} errorStack={errorInfo.errorStack} /> : <main id={`${props.name}-container`} />
  )
}


MicroFrontend.defaultProps = {
  document,
  window,
};


export default MicroFrontend
