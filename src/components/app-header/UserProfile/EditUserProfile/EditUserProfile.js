import React, { useState, useEffect } from 'react';
import MonadModal from 'ui/build/components/Modal/MonadModal';
import Para from "ui/build/components/Para/Para";
import Input from "ui/build/components/Inputs/Input";
import Button from 'ui/build/components/Button/Button';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { getUserDataByID } from './EditUserProfile.service';
import ChangePassword from './ChangePassword';
import './EditUserProfile.scss';
import { avatarList } from '../../../../utils/StaticData';

export default function EditUserProfile({ open, onCancel, options, userData, handleChangeFun, handleProfilePic, handleSaveProfilePic, selectedAvatar, btnDisable }) {

    // open avatar modal
    const [showUploadAvatar, setShowUploadAvatar] = useState(false);
    // set currently selected profile
    const [tempAvatarSelection, setTempAvatarSelection] = useState(userData.userPicURL);

    const onModalSubmit = (dialogData) => {
        options.onSubmit(dialogData);
    }

    const validationForm = Yup.object().shape({
        firstName: Yup.string()
            .required('FirstName is required'),
        mobileNo: Yup.string()
            .matches(/^[0-9\b]+$/, 'Mobile Number not valid')
            .min(10, 'Min 10 digits required')
            .max(10, 'Max 10 digits required')
    });

    return (
        <MonadModal
        className="profile-popup"
            variant="regular-modal"
            size="md"
            open={open}
            onSubmit={onModalSubmit}
            onCancel={onCancel}
            heading={options.heading}
            body={
                <Formik
                    initialValues={userData}
                    enableReinitialize={true}
                    onSubmit={(data, { setSubmitting }) => {
                        const updateBody = {};
                        updateBody['userId'] = data.userId;
                        updateBody['firstName'] = data.firstName;
                        updateBody['lastName'] = data.lastName;
                        updateBody['mobileNo'] = data.mobileNo;
                        onModalSubmit(data);
                        setSubmitting(false);
                    }}
                    validationSchema={validationForm}
                >
                    {({
                        values,
                        touched,
                        errors,
                        handleBlur,
                        handleSubmit,
                        setFieldValue
                    }) => (
                        <Form onSubmit={handleSubmit}>
                            <div className="profile-block row">
                                <div className="row w-100">
                                    <div className="col-9 pr-0">
                                        <div className="col-12 pb-1">
                                            <Input type="text"
                                                placeholder="First Name"
                                                name="firstName"
                                                label="First Name"
                                                autoComplete="off"
                                                isMandatory
                                                value={userData.firstName}
                                                onChange={e => handleChangeFun(e, setFieldValue)}
                                                onBlur={handleBlur}
                                            />
                                            {errors.firstName && touched.firstName && (
                                                <div className="error">{errors.firstName}</div>
                                            )}
                                        </div>
                                        <div className="col-12">
                                            <Input type="text"
                                                placeholder="Last Name"
                                                name="lastName"
                                                label="Last Name"
                                                autoComplete="off"
                                                value={userData.lastName}
                                                onChange={e => handleChangeFun(e, setFieldValue)}
                                            />
                                        </div>
                                        <div className="col-12">
                                            <Input type="text"
                                                placeholder="Email ID"
                                                name="emailId"
                                                label="Email ID"
                                                autoComplete="off"
                                                disabled={true}
                                                value={userData.emailId}
                                                onChange={e => handleChangeFun(e, setFieldValue)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-3 profile-section">
                                        <label>Profile Photo</label>
                                        <div>
                                            {
                                                selectedAvatar && selectedAvatar.trim() ?
                                                    <img className="img-design" src={`/assets/${selectedAvatar}.png`} alt="image" />
                                                    :
                                                    <img className="img-design" src="/assets/Default.png" alt="image" />
                                            }
                                        </div>
                                        <div className="text-center btn-size">
                                            <Button variant="outline" onClick={() => setShowUploadAvatar(true)}>Upload an Image</Button>

                                            <MonadModal
                                                variant="regular-modal"
                                                open={showUploadAvatar}
                                                submitText="Select"
                                                cancelText="Cancel"
                                                onSubmit={() => {
                                                    handleProfilePic(tempAvatarSelection)
                                                    setShowUploadAvatar(false)
                                                }}

                                                onCancel={() => {
                                                    setTempAvatarSelection(userData.userPicURL)
                                                    setShowUploadAvatar(false)
                                                }}

                                                heading="Choose an avatar"
                                                body={
                                                    <div className="avatar-list"
                                                    >
                                                        <React.Fragment>
                                                            {
                                                                avatarList.map((avatar) =>
                                                                    <img
                                                                        onClick={() => setTempAvatarSelection(avatar.alt)}
                                                                        className={`avatar-pic ${tempAvatarSelection === avatar.alt ? "selected-avatar" : ""}`}
                                                                        src={avatar.src} alt={avatar.alt}></img>
                                                                )
                                                            }
                                                        </React.Fragment>
                                                    </div>
                                                }
                                                footer={true}
                                            />
                                            <Para className="photo-remove" onClick={
                                                () => { handleProfilePic("") }
                                            }>Remove Photo</Para>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 pb-2">
                                    <ChangePassword></ChangePassword>
                                </div>
                                <div className="col-12">
                                    <Input type="text"
                                        placeholder="Mobile Number"
                                        name="mobileNo"
                                        label="Mobile Number"
                                        autoComplete="off"
                                        value={userData.mobileNo}
                                        onChange={e => handleChangeFun(e, setFieldValue)}
                                    />
                                    {errors.mobileNo && touched.mobileNo && (
                                        <div className="error">{errors.mobileNo}</div>
                                    )}
                                </div>
                                <div className="profile-actions d-flex justify-content-end w-100 pr">
                                    <Button variant="outline" onClick={onCancel}>Cancel</Button>
                                    <Button type="submit"
                                        onClick={() => {
                                            handleSaveProfilePic(selectedAvatar)
                                        }}
                                        disabled={btnDisable}>Save Changes</Button>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            }
        />
    )
}
