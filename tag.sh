#!/bin/bash
sed "s/latest/$1/g" deployment.yaml > rhea-service.yaml
sed "s/latest/$1/g" dev-deployment.yaml > dev-rhea-service.yaml
sed "s/latest/$1/g" sb-deployment.yaml > sb-rhea-service.yaml
sed "s/latest/$1/g" qa-deployment.yaml > qa-rhea-service.yaml
sed "s/latest/$1/g" llp-deployment.yaml > llp-rhea-service.yaml
