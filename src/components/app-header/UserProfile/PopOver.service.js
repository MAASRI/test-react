import axios from 'axios';

const umApiUrl = 'https://api.df-dev.net';

let loginData, sessionToken;
if (localStorage.getItem('loginResponse')) {
  loginData = JSON.parse(localStorage.getItem('loginResponse'));
  sessionToken = loginData.responseData.sessionToken;
}

let headers = {
  Authorization: sessionToken,
  'Content-Type': 'application/json',
};

export const getUserList = data => {
  return axios.post(`${umApiUrl}/user/list`, data, {
    headers: headers,
  });
};
