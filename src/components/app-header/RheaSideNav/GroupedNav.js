import React, { useEffect, useState } from 'react'
import { AiFillFolder, AiFillFolderOpen } from 'react-icons/ai';
import { Link } from 'react-router-dom'
import '../AppHeader.scss'


function GroupedNav({ item, active, setActiveListItem, RenderIcon }) {
  const [expanded, setExpanded] = useState(false)
  return (
    <div>
      <ul className="nav-list-item" onClick={() => setExpanded(!expanded)}>
          {
            expanded ? <AiFillFolderOpen /> : <AiFillFolder />
          }
          <p>
            {item.group}
          </p>
      </ul>
      <div className={`${expanded ? 'open-submenu' : 'close-submenu'}`}>
        {item.name.map((innerItem, index) => <li
          key={index}
          className={`nav-list-item ${active === innerItem.displayName
            ? 'nav-list-item-active'
            : ''}`}>
          <div className="submenu-list" onClick={e => setActiveListItem(e, innerItem.displayName)}>
            <Link key={index} to={innerItem.redirectUrl}>
              <RenderIcon icon={innerItem.icon} />
              <p>{innerItem.displayName}</p>
            </Link>
          </div>
        </li>)
        }
      </div>
    </div>)
}

export default GroupedNav
